
def get_robot_ids_from_infos(infos):
    robot_ids = [info["robot_manifest"]["robot_id"] for info in infos]
    return robot_ids
