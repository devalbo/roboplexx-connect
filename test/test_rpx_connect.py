
import sys, os
rpxconnect_dir = os.path.join(os.getcwd())
print "Looking for RPXConnect package in %s" % rpxconnect_dir
sys.path.append(rpxconnect_dir)

import rpxconnect, test_util

# connect to the Roboplexx website to get a list of available robots
rpxconnect.initialize(rpxconnect.RPX_API_KEY)
robots_info = rpxconnect.lookup_robots_info()
print "%s robots available now" % len(robots_info)

# find and select the first robot available
robot_ids = test_util.get_robot_ids_from_infos(robots_info)

print "Available robot IDs: %s" % robot_ids
robot_id = robot_ids[0]
robot = rpxconnect.get_robot_by_id(robot_id)
print "Selecting robot: %s" % robot.robot_name

# get access to the robot by requesting a lease
lease_response = robot.lease()

# move the robot forward for 5 seconds
print "Moving %s" % robot.robot_name
robot.set_speeds(33, 33, 5)

# it is polite to release the robot once you are done with it
robot.release_lease()

print "Done"
