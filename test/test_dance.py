
import sys, os
rpxconnect_dir = os.path.join(os.getcwd())
print "Looking for RPXConnect package in %s" % rpxconnect_dir
sys.path.append(rpxconnect_dir)

import rpxconnect

rpxconnect.initialize(rpxconnect.RPX_API_KEY)
robot = rpxconnect.get_robot_by_name('Devalbot')

lease_response = robot.lease()

robot.do_speed_commands(
    [(77, 77, 5),
    (50, -50, 3),
    (77, 77, 5),
    (50, -50, 3),
    (77, 77, 5),
    (50, -50, 3),
    (77, 77, 5),
    ])

robot.release_lease()

print "Done"
