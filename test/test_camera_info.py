
import sys, os
rpxconnect_dir = os.path.join(os.getcwd())
print "Looking for RPXConnect package in %s" % rpxconnect_dir
sys.path.append(rpxconnect_dir)

import rpxconnect

rpxconnect.initialize(rpxconnect.RPX_API_KEY)
robot = rpxconnect.get_robot_by_name('Devalbot')
print robot.robot_name
print robot.camera_names
print robot.get_snapshot_url_for_camera(robot.camera_names[0])

print "Done"
