
# Welcome to RPXConnect, the Python library for connecting to robots in [Roboplexx](http://www.roboplexx.com). #

With this library, you can write programs to move and control robots by remote control over the internet. This code 
and the [Roboplexx website](http://www.roboplexx.com) are under active development.

### To get started, : ###
* Making sure you have an account at [Roboplexx](http://www.roboplexx.com/intro/signup). If you don't, it's easy to sign up.
* Request an API key. Send requests to ajboehmler at gmail.com with the subject "Roboplexx API Key".
* Make sure you have [Python installed](http://www.python.org) on your machine.
* Downloading this code via git from [https://bitbucket.org/devalbo/roboplexx-connect](https://bitbucket.org/devalbo/roboplexx-connect)
* Run `pip install -r requirements.txt` to get the libraries required to run RPXConnect.
* From your Roboplexx profile page, generate an API key and replace the stub in the rpx/secrets.py file.
* Test it out by running `python test/test_rpx_connect.py` from inside your **roboplexx-connect** directory
* Have fun and let us know what you'd like to see next!

#### Comments, questions, and pull requests can be sent to ajboehmler at gmail.com ####