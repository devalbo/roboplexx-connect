
import datetime, time
from rpx_requests import make_rpx_api_get_request, make_rpx_api_post_request, make_robot_session_set_speeds_request
from urls import *


def robot_lease_required(fn):
    def check_robot_lease(rpx_robot, *args, **kwargs):
        if rpx_robot._robot_lease_token is None:
            raise Exception("Robot lease required to call %s()" % fn.__name__)
        if datetime.datetime.now() > rpx_robot._robot_lease_expiration:
            raise Exception("Robot lease expired")
        return fn(rpx_robot, *args, **kwargs)

    return check_robot_lease


class RpxRobot:

    def __init__(self, api_key, robot_info_json):
        self._api_key = api_key
        self._robot_info_json = robot_info_json["robot_manifest"]
        self._robot_lease_token = None
        self._robot_lease_expiration = None
        self._robot_lease_release_url = None

    def __repr__(self):
        return "<RpxRobot [%s]>" % self.robot_name

    # def __str__(self):
    #     return unicode(self).encode('utf-8')
    #
    # def __unicode__(self):
    #     return "<RpxRobot [%s]>" % self.robot_name

    @property
    def robot_info(self):
        return self._robot_info_json

    @property
    def robot_name(self):
        return self._robot_info_json["robot_name"]

    @property
    def lease_url(self):
        return "%s%s" % (ROBOPLEXX_URL, self._robot_info_json["robot_new_lease_url"])

    @property
    def release_lease_url(self):
        return "%s%s" % (ROBOPLEXX_URL, self._robot_lease_release_url)

    @property
    def mc_speed_dual_url(self):
        return self._robot_info_json["mc_speed_dual_url"]

    @property
    def cameras(self):
        return self._robot_info_json["cameras"]

    @property
    def camera_names(self):
        return [camera["name"] for camera in self.cameras]

    def get_snapshot_url_for_camera(self, camera_name):
        for camera in self.cameras:
            if camera["name"] == camera_name:
                return camera["snapshot_url"]

    @robot_lease_required
    def set_speeds(self, left_speed, right_speed, duration_in_secs=1.0):
        if duration_in_secs < 0:
            raise Exception("Invalid duration (must be 0 or greater): %s" % duration_in_secs)

        make_robot_session_set_speeds_request(self.mc_speed_dual_url, self._robot_lease_token, left_speed, right_speed)
        time.sleep(duration_in_secs)
        make_robot_session_set_speeds_request(self.mc_speed_dual_url, self._robot_lease_token, 0, 0)

    @robot_lease_required
    def do_speed_commands(self, command_list):
        for command in command_list:
            self.set_speeds(*command)

    @robot_lease_required
    def move_forward_full(self, duration):
        self.set_speeds(100, 100, duration)

    @robot_lease_required
    def move_forward_left(self, duration):
        self.set_speeds(50, 100, duration)

    @robot_lease_required
    def move_forward_right(self, duration):
        self.set_speeds(100, 50, duration)

    @robot_lease_required
    def stop(self):
        self.set_speeds(0, 0, 0)

    @robot_lease_required
    def pivot_left(self, duration):
        self.set_speeds(0, 100, duration)

    @robot_lease_required
    def pivot_right(self, duration):
        self.set_speeds(100, 0, duration)

    @robot_lease_required
    def move_reverse_full(self, duration):
        self.set_speeds(-100, -100, duration)

    @robot_lease_required
    def move_reverse_right(self, duration):
        self.set_speeds(-100, -50, duration)

    @robot_lease_required
    def move_reverse_left(self, duration):
        self.set_speeds(-50, -100, duration)

    def lease(self):
        lease_info = self._negotiate_lease()
        return lease_info

    def release_lease(self):
        self._release_lease()

    def _negotiate_lease(self):
        lease_response = make_rpx_api_get_request(self.lease_url)
        if lease_response.status_code == 200:
            self._robot_lease_token = lease_response.json()["lease_token"]

            lease_remainder_in_secs = lease_response.json()["lease_remainder_in_secs"]
            if lease_remainder_in_secs > 10:
                lease_remainder_in_secs -= 10
            else:
                lease_remainder_in_secs = 0

            self._robot_lease_expiration = datetime.datetime.now() + datetime.timedelta(seconds=lease_remainder_in_secs)

            self._robot_lease_release_url = lease_response.json()["release_lease_url"]
            return lease_response

        elif lease_response.status_code == 403:
            raise Exception("Access denied when getting lease")

        else:
            raise Exception("Error negotiating lease [%s]: %s" % (lease_response.status_code, lease_response))

    def _release_lease(self):
        response = make_rpx_api_post_request(self.release_lease_url)
        if response.status_code == 200:
            self._robot_lease_token = None
            self._robot_lease_expiration = None
            self._robot_lease_release_url = None

        else:
            raise Exception("Error releasing lease from %s [%s]: %s" % (self.release_lease_url,
                                                                        response.status_code,
                                                                        response))