
from functools import wraps


_api_key = None


def set_api_key(api_key):
    global _api_key
    _api_key = api_key


def get_api_key():
    return _api_key


def api_key_required(fn):
    """Requires an active API key"""
    @wraps(fn)
    def fn_requiring_initialization(*args, **kwargs):

        if _api_key is None:
            raise Exception("No API key has been set. Initialization is required.")

        return fn(*args, **kwargs)

    return fn_requiring_initialization


def robot_lease_required(fn):
    """Requires an active API key and a robot lease for a robot"""
    @wraps(fn)
    def fn_required_robot_lease(robot_id, *args, **kwargs):
        return fn(robot_id, *args, **kwargs)

    return fn_required_robot_lease