
# This is where your Roboplexx API key goes; you can find your Roboplexx API key on your Roboplexx profile page
# if you've been given API access (send requests to ajboehmler at gmail.com with the subject "Roboplexx API Key")
RPX_API_KEY = "<Your Roboplexx API key goes here>"
