
import requests
from decorators import get_api_key


def make_rpx_api_get_request(url, **requests_args):
    if "headers" not in requests_args:
        requests_args['headers'] = {}
    requests_args['headers']['Rpx-ApiKey'] = get_api_key()

    r = requests.get(url, **requests_args)
    return r


def make_rpx_api_post_request(url, **requests_args):
    if "headers" not in requests_args:
        requests_args['headers'] = {}
    requests_args['headers']['Rpx-ApiKey'] = get_api_key()

    r = requests.post(url, **requests_args)
    return r


def make_robot_session_set_speeds_request(mc_speed_dual_url, session_token, left_speed, right_speed):
    payload = {"left_speed": left_speed,
               "right_speed": right_speed}

    r = requests.post(mc_speed_dual_url,
                      data=payload,
                      headers={"Rpx-Session-Token": session_token})
    return r


            # $.ajax({
            #     url: self.robot_manifest.mc_speed_dual_url,
            #     method: 'POST',
            #     beforeSend: function (xhr) {
            #         xhr.setRequestHeader('Rpx-Session-Token', self.robot_manifest.session_token);
            #     },
            #     data: { left_speed: speedLeft, right_speed: speedRight},
            #     contentType: 'application/x-www-form-urlencoded',
            #     success: function () {
            #             self.lastSentSpeedLeft(speedLeft);
            #             self.lastSentSpeedRight(speedRight);
            #         },
            #     error: function (jqXHR, textStatus, errorThrown) {
            #         self._set_motor_speeds(self.lastSetSpeedLeft(), self.lastSetSpeedRight());
            #         if (jqXHR.status == 403) {
            #             self._check_if_user_active();
            #         }
            #     }
            # });
