
from decorators import api_key_required, set_api_key, get_api_key
from rpx_requests import make_rpx_api_get_request
from robot import RpxRobot
from urls import *


def initialize(api_key):
    set_api_key(api_key)


@api_key_required
def lookup_robots_info():
    robots_info_response = make_rpx_api_get_request(MANIFEST_URL)
    robots_info_json = robots_info_response.json()["robots"]

    return robots_info_json


@api_key_required
def get_robot_by_id(robot_id):
    robot_of_interest = None

    robots_info_json = lookup_robots_info()
    for robot_info_json in robots_info_json:
        if robot_info_json["robot_manifest"]["robot_id"] == robot_id:
            robot_of_interest = RpxRobot(get_api_key(), robot_info_json)
            break

    if robot_of_interest is None:
        raise ValueError("Unable to find robot with ID '%s' - this robot does not exist or is not available right now" % robot_id)

    return robot_of_interest


@api_key_required
def get_robot_by_name(robot_name):
    robot_of_interest = None

    robots_info_json = lookup_robots_info()
    for robot_info_json in robots_info_json:
        if robot_info_json["robot_manifest"]["robot_name"] == robot_name:
            robot_of_interest = RpxRobot(get_api_key(), robot_info_json)
            break

    if robot_of_interest is None:
        raise ValueError("Unable to find robot with name '%s' - this robot does not exist or is not available right now" % robot_name)

    return robot_of_interest


